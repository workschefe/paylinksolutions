<?php

namespace PaylinkSolutions\WSDL\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Request services
 * @subpackage Services
 */
class Request extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named requestXML
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \PaylinkSolutions\WSDL\structs\RequestXML $parameters
     * @return \PaylinkSolutions\WSDL\structs\ResponseXML|bool
     */
    public function requestXML(\PaylinkSolutions\WSDL\structs\RequestXML $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->requestXML($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \PaylinkSolutions\WSDL\structs\ResponseXML
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
