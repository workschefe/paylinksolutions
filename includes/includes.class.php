<?php

require_once ('vendor/autoload.php');
require_once ('controller.class.php');
require_once ('class/fizzBuzz.class.php');
require_once ('class/fibonacci.class.php');
require_once ('class/soapService.class.php');

/*** Carregar todas os ficheiros criados para a estrutura do wsdl ***/
$fullPath = "includes/wsdl/src/";

require_once ($fullPath . 'ClassMap.php');

foreach (glob($fullPath . 'services/*.php') as $filename) {
    include $fullPath . 'services/' . basename($filename);
}

foreach (glob($fullPath . "structs/*.php") as $filename) {
    include $fullPath . "structs/" . basename($filename);
}