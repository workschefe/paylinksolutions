<?php
    namespace PaylinkSolutions\Fibonacci;

    class FibonacciClass {

        public static function fibonacci($num) {
            $result = [];
            $first = 0;
            $second = 1; 
            $fb = 0;
            
            for ($i=0; $i <= $num; $i++) {
                if ($i == 1) {
                    $result[] = $fb; // porque a sequencia fibonacci o 1 repete-se. 
                } else {
                    $result[] = $fb;   
                    $fb = $first + $second;          
                    $first = $second;       
                    $second = $fb;
                }
            }
            return $result;
        }

    }
