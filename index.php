<?php
	require_once('includes/controller.class.php');
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<body id="page-top">
	<div id="container">
		<div class="container-fluid">
			<div class="d-sm-flex align-items-center justify-content-between mb-4">
				<h1 class="h3 mb-0 text-gray-800">Paylink Solutions</h1>
			</div>

			<div class="row">
				<div class="col-xl-3 col-md-6 mb-4">
					<div class="card border-left-primary shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
										Exercise FizzBuzz 1 - 20
									</div>
									<div class="h5 mb-0 font-weight-bold text-center">
										<a class="btn btn-primary" aria-expanded="false" href="index.php?FizzBuzz=true">FizzBuzz</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-3 col-md-6 mb-4">
					<div class="card border-left-success shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Exercise Fibonacci</div>
									<div class="h5 mb-0 font-weight-bold text-center">
										<form class="row" action="index.php" method="POST">
											<div class="col-xl-8 col-md-8 mb-8">
												<input type="number" name="numberFB" class="form-control form-control-user" placeholder="Enter Number">
											</div>
											<div class="col-xl-4 col-md-4 mb-4">
												<input type="submit" class="btn btn-primary" type="button" aria-expanded="false">
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-6 col-md-6 mb-4">
					<div class="card border-left-info shadow h-100 py-2">
						<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
									<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Exercise SOAP Service</div>
									<div class="row no-gutters align-items-center">
										<a style="margin: 5px;" class="btn btn-primary" aria-expanded="false" href="index.php?getPhysicians=true">get Physicians</a>
										<a style="margin: 5px;" class="btn btn-primary" aria-expanded="false" href="index.php?getSpecialty=true">get Specialty</a>
									</div>
								</div>
								<div class="col-auto">
									<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xl-12 col-lg-12">
					<div class="card shadow mb-4">
						<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Result List</h6>
						</div>
						<div class="card-body">
							<?php echo $listFE; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>