<?php

namespace PaylinkSolutions\WSDL\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for responseXML structs
 * @subpackage Structs
 */
class ResponseXML extends AbstractStructBase
{
    /**
     * The responseXMLResult
     * Meta informations extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \PaylinkSolutions\WSDL\structs\ResponseXMLResult
     */
    public $responseXMLResult;
    /**
     * Constructor method for responseXML
     * @uses ResponseXML::setResponseXMLResult()
     * @param \PaylinkSolutions\WSDL\structs\ResponseXMLResult $responseXMLResult
     */
    public function __construct(\PaylinkSolutions\WSDL\structs\ResponseXMLResult $responseXMLResult = null)
    {
        $this
            ->setResponseXMLResult($responseXMLResult);
    }
    /**
     * Get responseXMLResult value
     * @return \PaylinkSolutions\WSDL\structs\ResponseXMLResult|null
     */
    public function getResponseXMLResult()
    {
        return $this->responseXMLResult;
    }
    /**
     * Set responseXMLResult value
     * @param \PaylinkSolutions\WSDL\structs\ResponseXMLResult $responseXMLResult
     * @return \PaylinkSolutions\WSDL\structs\ResponseXML
     */
    public function setResponseXMLResult(\PaylinkSolutions\WSDL\structs\ResponseXMLResult $responseXMLResult = null)
    {
        $this->responseXMLResult = $responseXMLResult;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \PaylinkSolutions\WSDL\structs\ResponseXML
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
