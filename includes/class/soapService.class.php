<?php
    namespace PaylinkSolutions\SoapService;

    use \PaylinkSolutions\WSDL as wsdl;


    class SoapServiceClass {


        private static function callService($cMD, $pARAMS = null) {

            $options = array(
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'includes/wsdl/wsdl.wsdl',
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => wsdl\ClassMap::get(),
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CACHE_WSDL => true
            );

            $request    = new wsdl\services\Request($options);
            $parameters = new wsdl\structs\RequestXML($cMD, $pARAMS);
            $res        = $request->requestXML($parameters);
            $feedback   = ($res) ? $request->getLastResponse() : $request->getLastError();

            return self::checkFeedback($feedback, $res);
            
        }

        private static function checkFeedback($feedback, $res) {
            $feedback = (array) $feedback["PaylinkSolutions\WSDL\services\Request::requestXML"];

            if ($res) {
            } else {
                return array('Ocorreu um erro... '.$feedback["faultstring"]);
            }
        }



        public static function getPhysicians() {
            $cMD = 1;
            return self::callService($cMD);
        }

        public static function getPhysiciansBySpecialty() {
            
        }

        public static function getSpecialty() {
            $cMD = 2;
            return self::callService($cMD);
        }

    }