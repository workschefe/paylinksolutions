<?php

namespace PaylinkSolutions\WSDL\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for requestXML structs
 * @subpackage Structs
 */
class RequestXML extends AbstractStructBase
{
    /**
     * The CMD
     * Meta informations extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string
     */
    public $CMD;
    /**
     * The PARAMS
     * Meta informations extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string
     */
    public $PARAMS;
    /**
     * Constructor method for requestXML
     * @uses RequestXML::setCMD()
     * @uses RequestXML::setPARAMS()
     * @param string $cMD
     * @param string $pARAMS
     */
    public function __construct($cMD = null, $pARAMS = null)
    {
        $this
            ->setCMD($cMD)
            ->setPARAMS($pARAMS);
    }
    /**
     * Get CMD value
     * @return string|null
     */
    public function getCMD()
    {
        return $this->CMD;
    }
    /**
     * Set CMD value
     * @param string $cMD
     * @return \PaylinkSolutions\WSDL\structs\RequestXML
     */
    public function setCMD($cMD = null)
    {
        $this->CMD = $cMD;
        return $this;
    }
    /**
     * Get PARAMS value
     * @return string|null
     */
    public function getPARAMS()
    {
        return $this->PARAMS;
    }
    /**
     * Set PARAMS value
     * @param string $pARAMS
     * @return \PaylinkSolutions\WSDL\structs\RequestXML
     */
    public function setPARAMS($pARAMS = null)
    {
        $this->PARAMS = $pARAMS;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \PaylinkSolutions\WSDL\structs\RequestXML
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
