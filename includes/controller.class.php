<?php
    namespace PaylinkSolutions\Controller;
    require_once 'includes.class.php';

    use PaylinkSolutions\FizzBuzz\FizzBuzzClass as fizzbuzz;
    use PaylinkSolutions\Fibonacci\FibonacciClass as fibonacci;
    use PaylinkSolutions\SoapService\SoapServiceClass as soapSrv;

    $listFE = null;

    if (isset($_GET["FizzBuzz"]) && $_GET['FizzBuzz'] == 'true') {
        $listFE = createList(fizzbuzz::fizzBuzz(), 'FizzBuzz 1 to 20');
    }

    if (isset($_POST["numberFB"]) && is_numeric($_POST['numberFB'])) {
        $listFE = createList(fibonacci::fibonacci($_POST['numberFB']), 'Fibonacci' );
    }

    if (isset($_GET["getPhysicians"]) && $_GET['getPhysicians'] == 'true') {
        $listFE = createList(soapSrv::getPhysicians(), 'Physicians List');
    }

    if (isset($_GET["getSpecialty"]) && $_GET['getSpecialty'] == 'true') {
        $listFE = createList(soapSrv::getSpecialty(), 'Specialty List');
    }
    

    function createList($data, $title) {
        $list = '
        <div style="margin-top: 10px;">
            <h4 class="text-center"> '.$title.' </h4>
        </div>
        <div>
            <ul>';
        
        foreach ($data as $item) {
            $list .= '
            <li>
                <span>'.$item.'</span>
            </li>';
        }
    
        $list .= '</ul></div>';

        return $list;
    }
    