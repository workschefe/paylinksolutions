<?php
    namespace PaylinkSolutions\FizzBuzz;

    class FizzBuzzClass {

        public static function fizzBuzz() {
            $result = [];
            for ($var = 1; $var <= 20; $var++) {
                if ($var % 3 == 0 && $var % 5 == 0) {
                    $result[] = 'FizzBuzz';
                    continue;
                }
                if ($var % 3 == 0) {
                    $result[] = 'Fizz';
                    continue;
                }  
                if ($var % 5 == 0) {
                    $result[] = 'Buzz';
                    continue;
                }  
                if ($var % 3 != 0 || $var % 5 != 0) {
                    $result[] = $var;
                }
            }
            return $result;
        }

        public static function fizzBuzzOnDemand($var1, $var2) {
            if (is_numeric($var1) && is_numeric($var2)) {
                $result = [];
                while($var1 <= $var2) {
                    if ($var1 % 3 == 0 && $var1 % 5 == 0) {
                        $result[] = 'FizzBuzz';
                        continue;
                    }
                    if ($var1 % 3 == 0) {
                        $result[] = 'Fizz';
                        continue;
                    }  
                    if ($var1 % 5 == 0) {
                        $result[] = 'Buzz';
                        continue;
                    }  
                    if ($var1 % 3 != 0 || $var1 % 5 != 0) {
                        $result[] = $var1;
                    }
                    $var1++;
                } 
            } else {
                return null;
            }
            return $result;

        }

    }
